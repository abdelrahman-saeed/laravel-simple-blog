@extends('admin.layouts.layout')

@section('title')
    اضافة مستخدم
@endsection

@section('header')

@endsection

@section('content')

    <section class="content-header">
        <h1>
            المستخدمين
            <small>اضافة مستخدم</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/adminpanel')}}"><i class="fa fa-dashboard"></i> الرئيسيه</a></li>
            <li><a href="{{url('/adminpanel/users')}}"><i class="fa fa-users"></i> عرض المستخدمين</a></li>

            <li class="active">اضافة مستخدم</li>

        </ol>
    </section>


    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <form method="POST" action="{{ url('adminpanel/users') }}">

                            @csrf

                                @include('admin.user.form')

                        </form>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            </div>
            <!-- /.row -->
    </section>


@endsection


@section('footer')

@endsection