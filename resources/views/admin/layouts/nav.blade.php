<li >
    <a href="{{ url('/adminpanel') }}">
        <i class="fa fa-dashboard"></i> <span>الرئيسيه</span>
    </a>
        {{--<li class="active"><a href="{{ url('/adminpanel') }}"><i class="fa fa-circle-o"></i> الرئيسيه</a></li>--}}
</li>



<li class="treeview">
    <a href="#">
        <i class="fa fa-users"></i> <span>المستخدمين</span>
        <span class="pull-left-container">
              <i class="fa fa-angle-left pull-left"></i>
            </span>
    </a>
    <ul class="treeview-menu">

        <li class=""><a href="{{ url('/adminpanel/users') }}"><i class="fa fa-users"></i> عرض المستخدمين</a></li>
        <li class=""><a href="{{ url('/adminpanel/users/create') }}"><i class="fa fa-user-plus"></i> اضافة مستخدم</a></li>

    </ul>
</li>


<li class="treeview">
    <a href="#">
        <i class="fa fa-newspaper-o"></i> <span>المدونات</span>
        <span class="pull-left-container">
              <i class="fa fa-angle-left pull-left"></i>
            </span>
    </a>
    <ul class="treeview-menu">

        <li class=""><a href="{{ url('/adminpanel/blog') }}"><i class="fa fa-newspaper-o"></i> عرض المدونات</a></li>
        <li class=""><a href="{{ url('/adminpanel/blog/create') }}"><i class="fa fa-plus-circle"></i> اضافة مدونه</a></li>

    </ul>
</li>

<li class="treeview">
    <a href="#">
        <i class="fa fa-book"></i> <span>الاقسام</span>
        <span class="pull-left-container">
              <i class="fa fa-angle-left pull-left"></i>
            </span>
    </a>
    <ul class="treeview-menu">

        <li class=""><a href="{{ url('/adminpanel/category') }}"><i class="fa fa-book"></i> عرض الاقسام</a></li>
        <li class=""><a href="{{ url('/adminpanel/category/create') }}"><i class="fa fa-plus-circle"></i> اضافة قسم</a></li>

    </ul>
</li>

<li class="treeview">
    <a href="#">
        <i class="fa fa-tags"></i> <span>التاجات</span>
        <span class="pull-left-container">
              <i class="fa fa-angle-left pull-left"></i>
            </span>
    </a>
    <ul class="treeview-menu">

        <li class=""><a href="{{ url('/adminpanel/tags') }}"><i class="fa fa-tags"></i> عرض التاجات</a></li>
        <li class=""><a href="{{ url('/adminpanel/tags/create') }}"><i class="fa fa-plus-circle"></i> اضافة تاج</a></li>

    </ul>
</li>