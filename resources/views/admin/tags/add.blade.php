@extends('admin.layouts.layout')

@section('title')
    اضافة تاج
@endsection

@section('header')

@endsection

@section('content')

    <section class="content-header">
        <h1>
            التاجات
            <small>اضافة تاج</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/adminpanel')}}"><i class="fa fa-dashboard"></i> الرئيسيه</a></li>
            <li><a href="{{url('/adminpanel/tags')}}"><i class="fa fa-users"></i> عرض التاجات</a></li>

            <li class="active">اضافة تاج</li>

        </ol>
    </section>


    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <form method="POST" action="{{ url('adminpanel/tags') }}">

                            @csrf

                                @include('admin.tags.form')

                        </form>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            </div>
            <!-- /.row -->
    </section>


@endsection


@section('footer')

@endsection