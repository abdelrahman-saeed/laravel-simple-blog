
@extends('admin.layouts.layout')

@section('title')
    تعديل تاج
    {{$tag->name}}
@endsection

@section('header')

@endsection

@section('content')

    <section class="content-header">
        <h1>
            التاجات
            <small>   تعديل تاج  {{ $tag->name }}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/adminpanel')}}"><i class="fa fa-dashboard"></i> الرئيسيه</a></li>
            <li><a href="{{url('/adminpanel/tags')}}"><i class="fa fa-users"></i> عرض التاجات</a></li>

            <li class="active">
                  تعديل تاج</li>
            {{$tag->name}}
        </ol>
    </section>


    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">تعديل تاج</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">


                       {!! Form::model($tag,array('route' =>array('tags.update',$tag->id), 'method'=>'patch')) !!}

                            @include('admin.tags.form')

                       {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


@section('footer')

@endsection