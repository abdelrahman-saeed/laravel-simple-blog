


    <div class="form-group row">
        <label style="float: none;" for="name" class="col-md-6 text-right">عنوان التدوينه</label>

        <div style="float: none;" class="col-md-6">
            {!! Form::text("title",null,['class'=>'form-control']) !!}

            @if ($errors->has('title'))
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div  class="form-group row">
        <label style="float: none;" for="email" class="col-md-6 text-right">وصف مختصر</label>

        <div style="float: none;" class="col-md-6">
            {!! Form::textarea("short_desc",null,['class'=>'form-control', 'rows'=>'3']) !!}

            @if ($errors->has('short_desc'))
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('short_desc') }}</strong>
                                    </span>
            @endif
        </div>
    </div>


    <div  class="form-group row">
        <label style="float: none;" for="email" class="col-md-6 text-right">محتوى التدوينه</label>

        <div style="float: none;" class="col-md-6">
            {!! Form::textarea("content",null,['class'=>'form-control']) !!}

            @if ($errors->has('content'))
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div  class="form-group row">
        <label style="float: none;" for="category" class="col-md-6 text-right">القسم</label>

        <div style="float: none;" class="col-md-6">
            {{--{!! Form::select("category",$categories,null,['class'=>'form-control']) !!}--}}

            <select class="form-control" name="category_id">
                @if(count($categories)>0)
                @foreach($categories as $cat)
                    @if(isset($blog)&& $blog->category_id == $cat->id)
                            <option value="{{$cat->id}}" selected>{{$cat->name}}</option>
                     @else
                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                    @endif
                 @endforeach
                @endif

            </select>


            @if ($errors->has('category'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('category') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div  class="form-group row">
        <label style="float: none;" for="tag_id" class="col-md-6 text-right">التاجات</label>

        <div style="float: none;" class="col-md-6">

        <select name="tag_id[]" class="form-control select_tags"  multiple="multiple" data-placeholder=" يمكنك تحديد اكثر من تاج"
                style="width: 100%;">
            @if(!empty($tags))
                @foreach($tags as $tag)
                    @if(isset($blog) && !empty($blog->tags))
                        <?php
                        $b_tags = array_values((array)$blog->tags()->allRelatedIds());
                        ?>
                        @if(in_array($tag->id,$b_tags[0]))
                                <option value="{{$tag->id}}" selected>{{$tag->name}}</option>
                        @else
                                <option value="{{$tag->id}}">{{$tag->name}}</option>
                        @endif

                    @else
                        <option value="{{$tag->id}}">{{$tag->name}}</option>
                    @endif
                @endforeach
            @endif

        </select>

        </div>
<!--
        {{--<div style="float: none;" class="col-md-6">--}}
            {{--{!! Form::select("category",$categories,null,['class'=>'form-control']) !!}--}}


                {{--@if(!empty($tags))--}}
                    {{--@foreach($tags as $tag)--}}
                          {{--@if(isset($blog) && !empty($blog->tags))--}}
                        {{--<?php--}}
                        {{--$b_tags = array_values((array)$blog->tags()->allRelatedIds());--}}
                        {{--?>--}}
                                  {{--@if(in_array($tag->id,$b_tags[0]))--}}
                   {{--<input type="checkbox" name="tag_id[]" value="{{$tag->id}}"checked> {{$tag->name}} |--}}
                                      {{--@else--}}
                    {{--<input type="checkbox" name="tag_id[]" value="{{$tag->id}}"> {{$tag->name}} |--}}
                                  {{--@endif--}}

                    {{--@else--}}
                    {{--<input type="checkbox" name="tag_id[]" value="{{$tag->id}}"> {{$tag->name}} |--}}
                          {{--@endif--}}
                    {{--@endforeach--}}
                {{--@endif--}}

            {{--@if ($errors->has('tag_id'))--}}
                {{--<span class="invalid-feedback" role="alert">--}}
                    {{--<strong>{{ $errors->first('tag_id') }}</strong>--}}
                {{--</span>--}}
            {{--@endif--}}
        {{--</div>--}}
  -->
    </div>

    <div class="form-group row">
    <label style="float: none;" for="image" class="col-md-6 text-right">صورة التدوينه</label>

    <div style="float: none;" class="col-md-6">

        @if(isset($blog) && (!empty($blog->image)) && file_exists('images/'.$blog->image))

            <img class="img-bordered" src="{{url('images/'.$blog->image)}}" height="200" width="200" >
            <hr>
        @endif
        {!! Form::file("image",null,['class'=>'form-control']) !!}

        @if ($errors->has('image'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('image') }}</strong>
            </span>
        @endif
    </div>
    </div>



    <div class="form-group row mb-0">
        <div style="float: none;"  class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-success">
                حفظ
            </button>
        </div>
    </div>


