@extends('admin.layouts.layout')

@section('title')
    المدونات
@endsection

@section('header')
    {!! Html::style('/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}
@endsection

@section('content')

    <section class="content-header">
        <h1>
            المدونات
            <small>عرض المدونات</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/adminpanel')}}"><i class="fa fa-dashboard"></i> الرئيسيه</a></li>
            <li class="active">عرض المدونات</li>
        </ol>



    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th class="text-center" >#</th>
                                <th class="text-center" >عنوان المدونه</th>

                                <th class="text-center" >اضيفت فى </th>
                                <th class="text-center" >القسم</th>
                                <th class="text-center" >التاجات</th>
                                <th class="text-center" >التحكم</th>
                            </tr>
                            </thead>
                            <tbody>
                @foreach($blogs as $blog)
                            <tr>
                                <td>{{$blog->id}}</td>
                                <td>{!! \Illuminate\Support\Str::words($blog->title,5,'...')!!}</td>
                                <td>{{$blog->created_at}}</td>
                                <td>
                                    <a href="{{url('adminpanel/blog/blogCategory/'.$blog->category['id'])}}">{{$blog->category['name'] }}</a>
                                </td>
                                <td>
                                    @foreach($blog->tags as $tag)
                                       <a href="{{url('adminpanel/blog/tagBlog/'.$tag->name)}}"><span class="badge bg-green">{{$tag->name}}</span></a>
                                    @endforeach
                                </td>

                                <td class="text-center">
                                <a class="btn btn-info" href="{{url('/adminpanel/blog/'.$blog->id.'/edit')}}">
                                    <i class="fa fa-pencil-square" ></i>
                                </a>

                                    <a class="btn btn-danger" onclick="return confirm('هل انت متأكد ؟!')" href="{{url('/adminpanel/blog/'.$blog->id.'/delete')}}">
                                        <i class="fa fa-trash" ></i>
                                    </a>
                                </td>
                            </tr>
                    @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->


@endsection


@section('footer')
    <!-- DataTables -->
    {!! Html::script('/admin/bower_components/datatables.net/js/jquery.dataTables.min.js') !!}
    {!! Html::script('/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}


    <script>
        $(function () {

            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true
            })
        })
    </script>
@endsection