@extends('layouts.app')
@section('title')
    المدونه
@endsection




@section('content')

    <div class="about">
        <div class="container">
            <section class="title-section">
                <h1 class="title-header">المدونه
                    @if(!empty($text))
                        / {{$text}}
                    @endif
                </h1>

            </section>
        </div>
    </div>


        <div class="container">

            <div class="row ">

                <div class="col-lg-3">
                    @if(!empty($cats))
                    <div class="block-heading-two">
                        <h3><span><i class="fa fa-flag"></i> الأقسام </span></h3>
                    </div>
                    <div class="panel-group" id="accordion-alt3">
                   @foreach($cats as $cate)
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title"> <a  href="{{url('blog/'.$cate->name)}}">  {{$cate->name}} <i class="fa fa-angle-left"></i></a> </h4>
                            </div>
                        </div>
                   @endforeach
                     </div>
                    @endif
                    @if(!empty($tags))
                    <div class="block-heading-two">
                        <h3><span><i class="fa fa-tags"></i>   التاجات </span></h3>
                    </div>

                    @foreach($tags as $tag)
                        <a href="{{url('blog/'.$tag->name)}}"> <span style="font-size: 15px;background-color: #23a17e;" class="badge badge-info"> {{$tag->name}} </span> </a>
                    @endforeach

                   @endif
                </div>

                <div class="col-lg-9">
           @if(!empty($blogs) && count($blogs)>0)
               <?php $x = 0 ?>
               @foreach($blogs as $blog)
           <?php $i = $x++ ?>
            <div class="row">
                <div class="col-md-9">
                    <div class="block-heading-two">
                        <h2><span>{!! \Illuminate\Support\Str::words($blog->title,15,'...')!!}</span></h2>
                    </div>
                    <p>
                        {!! \Illuminate\Support\Str::words($blog->short_desc,30,'...')!!}
                        <br>
                         <i  class="fa fa-clock-o"></i>  {{$blog->created_at}}  <br>
                        <i  class="fa fa-flag"></i> <a href="{{url('blog/'.$blog['category']->name)}}"> {{$blog['category']->name}} </a><br>
                        @if(!empty($blog->tags))
                        <i class="fa fa-tags"></i>
                            @foreach($blog->tags as $tag)
                                <a href="{{url('blog/'.$tag->name)}}"> <span style="font-size: 15px;background-color: #23a17e;" class="badge bg-success"> {{$tag->name}} </span> </a>
                            @endforeach
                        @endif
                    </p>

                    <a class="banner_btn" href="{{url('blog/details/'.$blog->id)}}">التفاصيل</a>
                </div>

                <div class="col-md-3">
                    <img  class="img-bordered" width="198px" height="230px" src="{{url('images/'.$blog->image)}}" >
                </div>

            </div>
                 @if(count($blogs)!== 0 && ($i+1)/count($blogs)  !== 1)
                   <br>
                   <hr>
                  @endif
               @endforeach

               @else
                        <div class="alert alert-danger fade in alert-dismissible text-center">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            عذرا لا توجد نتائج !!
                        </div>

            @endif
        </div>



    </div>



<div class="row text-center">
    @if(!empty($text))
        {{--{{ $blogs->setpath('/blog/'.$text)->render()}}--}}
        {{ $blogs->setpath('/blog/'.$text)->links('vendor.pagination.custom')}}
    @else
    {{--{!! $blogs->render()!!}--}}
        {{ $blogs->links('vendor.pagination.custom') }}
    @endif
</div>



</div>

   <br>

@endsection


