<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//  admin routs :



    Route::group(['middleware'=>['admin','web']], function (){

    Route::get('adminpanel','AdminController@index');

  //users

    Route::resource('adminpanel/users','UsersController');
    Route::post('adminpanel/users/changePassword','UsersController@updatePassword');
    Route::get('adminpanel/users/{id}/delete','UsersController@destroy');


    //blog
    Route::resource('adminpanel/blog','BlogController');
    Route::get('adminpanel/blog/{id}/delete','BlogController@destroy');
    Route::get('adminpanel/blog/blogCategory/{id}','BlogController@bolgCategory');
    Route::get('adminpanel/blog/tagBlog/{id}','BlogController@tagBlog');

   //category
    Route::resource('adminpanel/category','CategoryController');
    Route::get('adminpanel/category/{id}/delete','CategoryController@destroy');

    //tags
    Route::resource('adminpanel/tags','TagsController');
    Route::get('adminpanel/tags/{id}/delete','TagsController@destroy');



}
);







//user routs :
//
//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/home', 'BlogController@home')->name('home');
Route::get('/', 'BlogController@home')->name('home');

//blog routs
Route::get('blog', 'BlogController@allBlog')->name('Blog');
Route::get('blog/{text}', 'BlogController@blogSearch');
Route::post('search', 'BlogController@blogSearch');
Route::get('blog/details/{id}', 'BlogController@oneBlog');


//comments routs
Route::post('comment/create', 'CommentController@store');
Route::get('comment/load_data', 'CommentController@load_data')->name('comment.load_data');