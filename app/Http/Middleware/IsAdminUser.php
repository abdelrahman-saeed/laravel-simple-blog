<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdminUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guest()){
            return redirect('/adminpanel/login');
        }

        if(Auth::user()->admin != 1){
            return redirect('/adminpanel/login')->with('error','البيانات غير صحيحه');
        }

        return $next($request);
    }
}
