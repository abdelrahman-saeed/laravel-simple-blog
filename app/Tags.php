<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    //

    protected $table = 'tags';

    protected $fillable = [
        'name'
    ];


    function blog(){
        return $this->belongsToMany('App\Tags','link_tags','tag_id','blog_id');
    }
}
